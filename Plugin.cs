﻿/*
    Copyright 2024 Gironta

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

using System;
using BepInEx;
using HarmonyLib;
using Live2D.Cubism.Core;
using UnityEngine;
using UnityEngine.UI;

namespace ParameterCustomizer
{
    [BepInPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
    public class Plugin : BaseUnityPlugin
    {
        private void Awake()
        {
            // Plugin startup logic
            Logger.LogInfo($"Plugin {PluginInfo.PLUGIN_GUID} is loaded!");
            var harmony = new Harmony(PluginInfo.PLUGIN_GUID);
            harmony.PatchAll();
        }
    }

    [HarmonyPatch(typeof(GameStates))]
    [HarmonyPatch("Start")]
    class MyGameStatesPatch
    {
        static void Postfix()
        {
            string[] flowChartParameters = new string[] { "Fullness", "Futa", "Corruption", "Breast" };
            MakeParameterCyclingButtons(flowChartParameters);
        }

        static void MakeParameterCyclingButtons(string[] parameters)
        {
            Button eyeButton = Array.Find(GameObject.FindObjectsOfType<Button>(), b => b.name == "EyeButton");
            var transform = eyeButton.transform;
            for (int i = 0; i < parameters.Length; i++)
            {
                var buttonCopy = UnityEngine.Object.Instantiate(transform.gameObject, transform.parent, false);
                buttonCopy.transform.localPosition = transform.localPosition + new Vector3(0, -100f - (50f * i), 0);
                var btn = buttonCopy.GetComponent<Button>();
                btn.onClick.RemoveAllListeners();
                for (var j = 0; j < btn.onClick.GetPersistentEventCount(); j++)
                {
                    btn.onClick.SetPersistentListenerState(j, UnityEngine.Events.UnityEventCallState.Off);
                }
                // Have to make a local copy for the arrow function
                int index = i;
                btn.onClick.AddListener(() =>
                    {
                        var flowchart = GameObject.FindObjectsOfType<Fungus.Flowchart>()[1];
                        var parameter = flowchart.GetIntegerVariable(parameters[index]);
                        flowchart.SetIntegerVariable(parameters[index], (parameter + 1) % 3);
                    });
            }
        }
    }

    [HarmonyPatch(typeof(SexAnimationHelper))]
    [HarmonyPatch("LateUpdate")]
    class MySexAnimationHelperPatch
    {
        static void Postfix(CubismModel ___Model)
        {
            var flowchart = GameObject.FindObjectsOfType<Fungus.Flowchart>()[1];
            int fullness = flowchart.GetIntegerVariable("Fullness");
            ___Model.Parameters.FindById("ParamBoteLevels").Value = fullness;
            int futa = flowchart.GetIntegerVariable("Futa");
            ___Model.Parameters.FindById("ParamFutaLevels").Value = futa;
            int corruption = flowchart.GetIntegerVariable("Corruption");
            ___Model.Parameters.FindById("ParamCorruptionLevel").Value = corruption;
            int breast = flowchart.GetIntegerVariable("Breast");
            ___Model.Parameters.FindById("ParamBoobLevels").Value = breast;
        }
    }

}
